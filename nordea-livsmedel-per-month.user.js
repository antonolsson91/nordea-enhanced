// ==UserScript==
// @name         Nordea Livsmedel per månad
// @namespace    anton-olsson@outlook.com
// @version      1.0
// @description  Gjord av Anton, 2018-02-12
// @author       You
// @require      https://code.jquery.com/jquery-latest.min.js
//               https://github.com/julien-maurel/js-storage
// @require      https://raw.githubusercontent.com/julien-maurel/js-storage/master/js.storage.min.js
// @match        https://internetbanken.privat.nordea.se/nsp/*
// @grant        none
// ==/UserScript==

Storage.prototype.init = function(id, defaultValue){
    var item;
    if(typeof localStorage.getItem(id) == "undefined"){
        localStorage.setItem(id, defaultValue);
    } else {
        item = localStorage.getItem(id);

        if(typeof defaultValue == "object"){
            var error = false;
            try {
                item = JSON.parse( localStorage.getItem(id) );
            } catch(e) {
                error = true;
            }

            if( error ){
                console.error("There was an error in your saved object.");
                console.log( localStorage.getItem(id) );
                localStorage.setItem(id, defaultValue);
            } else {
                item = JSON.parse( localStorage.getItem(id) );
            }
        }
    }
    return item;
};

jQuery(document).ready(function(){

    jQuery.fn.extend({
        addController: function( options ) {
            return this.each(function(i, e) {
                let controller, styling = "";
                /* if options.for already exists */ if( $( "#" + options.for ).length ) { let random = Math.round(Math.random()*100); options.for = `${options.for}${random}`; }

                if( options.type.toLowerCase() == "checkbox" ){
                    controller = `<input type="checkbox" id="${options.for}-controller" style="${options.style}">`;
                } else if( options.type.toLowerCase() == "range" ){
                    styling = `
input#myStorage-controller[type="range"]:before {
content: attr(min);
color: initial;
}
input#myStorage-controller[type="range"]:after {
content: attr(max);
color: initial;
}`;
                    controller = `<input type="range" min="${options.between[0]}" max="${options.between[1]}" id="${options.for}-controller" style="${options.style}">`;
                }

                let container = `<div id="${options.for}-container" style="${options.container_style}"><style>${styling}</style><label for="${options.for}-controller">${options.label}</label>${controller}</div>`;
                $(e).prepend(container);

                if( options.type.toLowerCase() == "checkbox" ){
                    $(`#${options.for}-container`).on("change",`#${options.for}-controller`, function(e){
                        e.preventDefault();
                        let $this = $(this);
                        if( $this.is( ":checked" ) ){
                            options.on();
                        } else {
                            options.off();
                        }
                    } );
                } else if( options.type.toLowerCase() == "range" ){
                    $(`#${options.for}-container`).on("change",`#${options.for}-controller`, function(e){
                        e.preventDefault();
                        let $this = $(this);
                        let val = $this.val();
                        $this.attr("title", val);
                        options.on(val);
                    } );
                }

            });
        }
    });

    //.addController = function( options ){

    var key = localStorage.init("myStorage", {});
    /*
    $("body").addController({
        for: "myStorage",
        type: "checkbox",
        label: "Test",
        style: `padding: 15px;`,
        container_style: `background: orange;`,
        on: function(){ console.log( "on" ); },
        off: function(){ console.log( "off" ); }
    });
*/


    $("body").addController({
        for: "myStorage",
        type: "range",
        between: [0,90],
        label: "Test",
        style: `padding: 15px;`,
        container_style: `background: orange;`,
        on: function(val){ countGroceries( val ); }
    });

    Date.prototype.addDays = function(days) {
        var dat = new Date(this.valueOf());
        dat.setDate(dat.getDate() + days);
        return dat;
    };
    Date.prototype.removeDays = function(days) {
        var dat = new Date(this.valueOf());
        dat.setDate(dat.getDate() - days);
        return dat;
    };

    function countGroceries(days){

        window.obj = {};

        $("#transactionstable tr:nth-child(1), #transactionstable tr:nth-child(2)").addClass("ignore");
        $("#transactionstable tr:not(.ignore)").each(function(i0,e0){
            try{

                var $this0 = $(e0);
                var rad = $this0.index();

                $this0.find("td").each(function(i1,e1){
                    var $this = $(e1);
                    let $i = $(e1).index();

                    $(e1).addClass( "" + $i + "" );
                });

                var datum = $this0.find(".1").html().trim();
                var transaktion = $this0.find(".2 a").html().trim();
                var belopp = parseFloat($this0.find(".4").html().trim().replace(".",""));
                var saldo = parseFloat($this0.find(".5").html().trim().replace(".",""));

                window.obj[rad] = [datum, transaktion, belopp, saldo];

            }catch(e){console.info(i0, e);}
        });


        var livsmedelsbutiker = ["ica","hemkop","hemköp","tempo","willys","coop","vår krog","subway","mcdonalds","restaurang","lidl","burger king", "max", "pizza hut", ];
        var livsmedel = 0;

        let then = new Date().removeDays(parseInt(days));
        let now = new Date();

        for(let arr in obj){
            let curr = obj[arr];
            let datum = curr[0];
            let transaktion = curr[1];
            let belopp = curr[2];
            let ta = transaktion.toLowerCase();

            now = new Date(datum);

            if(then < now){
                for(let i = 0; i < livsmedelsbutiker.length; i++){
                    if(ta.includes(livsmedelsbutiker[i])){livsmedel += belopp;}
                }
            }
        }
        now = new Date();
        console.log(`Lagt ${Math.abs(livsmedel)} på livsmedel mellan ${now} och ${then} (${days} dagar)`);


    }
});
